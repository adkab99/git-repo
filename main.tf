
#Creating ec2 instance

resource "aws_instance" "ec2_example" {
    ami = var.ami 
    instance_type = var.instance_type
    tags = {
      Name ="${terraform.workspace}-instance"
    }
}

# Creating VPC
resource "aws_vpc" "demovpc" {
  cidr_block       = "10.50.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "Demo-VPC"
  }
}

resource "aws_instance" "ade" {
  ami=var.ami
  instance_type = var.instance_type
  
}
